import java.util.Scanner;

public class BAi_4 {

    public static void main(String[] args) {
        try {
            boolean check = true;
            Scanner scanner = new Scanner(System.in);
            System.out.println("Nhap ho va ten :");
            String fullname = scanner.nextLine();

            for (int i = 0; i < fullname.length(); i++) {
                char charAt = fullname.charAt(i);
                if ((!Character.isLetter(charAt) && charAt != 32) || (charAt == 32 && charAt == fullname.charAt(i-1))) {
                    check = false;
                    break;
                }
            }

            if (check) {
                System.out.println(fullname);
            }
        } catch (RuntimeException rex) {
            rex.printStackTrace();
        }

    }
}
